import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './../components/dashboard/dashboard.component';
import { CharactersComponent } from './../components/characters/characters.component';
import { CharacterDetailComponent } from './../components/characters/character-detail/character-detail.component';
import { NotFoundComponent } from '../components/errors/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'characters', component: DashboardComponent },
  { path: 'characters/:charType', component: CharactersComponent, children: [{ path: ':id', component: CharacterDetailComponent }] },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
]

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
