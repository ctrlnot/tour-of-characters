import { CharacterBaseForm } from './character-base-form';

export class CharacterTextboxForm extends CharacterBaseForm<string> {
  controlType = 'textbox';
  type: string;
  pattern: string;
  
  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}