import { CharacterBaseForm } from './character-base-form';

export class CharacterDropdownForm extends CharacterBaseForm<string> {
  controlType = 'dropdown';
  options: {
    key: string,
    value: string
  }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || '';
  }
}