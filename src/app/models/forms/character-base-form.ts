export class CharacterBaseForm<T> {
  value: T;
  key: string;
  label: string;
  required: boolean;
  controlType: string;
  validationPattern: string;

  constructor(
    options: {
      value?: T,
      key?: string,
      label?: string,
      required?: boolean,
      controlType?: string,
      validationPattern?: string
    } = {}
  ) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.controlType = options.controlType || '';
    this.validationPattern = options.validationPattern || '';
  }
}