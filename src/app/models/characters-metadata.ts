import { Character } from './character';

const charactersMetadata = {
  heroes: [
    {
      type: 'hidden',
      key: 'id',
      label: 'Id'
    },
    {
      type: 'text',
      key: 'name',
      label: 'Hero Name',
      value: '',
      required: true,
      validationPattern: '[a-zA-Z0-9 ]+'
    },
    {
      type: 'text',
      key: 'superpower',
      label: 'Hero Superpower',
      value: '',
      required: false
    },
    {
      type: 'dropdown',
      key: 'rating',
      label: 'Rating',
      options: [
        {key: 'solid',  value: 'Solid'},
        {key: 'great',  value: 'Great'},
        {key: 'good',   value: 'Good'},
        {key: 'unproven', value: 'Unproven'}
      ]
    }
  ],
  villains: [
    {
      type: 'hidden',
      key: 'id',
      label: 'Id'
    },
    {
      type: 'text',
      key: 'name',
      label: 'Villain Name',
      value: '',
      required: true,
      validationPattern: '[a-zA-Z0-9 ]+'
    },
    {
      type: 'text',
      key: 'superpower',
      label: 'Villain Superpower',
      value: '',
      required: false
    }
  ]
};

export function getAllCharacterTypes() {
  return Object.keys(charactersMetadata);
}

export function getCharacterKeys(charType: string) {
  let keys = [];
  charactersMetadata[charType].forEach(metadatum => keys.push(metadatum.key));

  return keys;
}

export function getCharacterMetadata(charType: string) {
  return charactersMetadata[charType];
}

export function getAllCharactersMetadata() {
  return charactersMetadata;
}

export function getCharacterDefaultModelValue(charType: string): Character {
  let defaultModelValue: Character = { id: null, name: '', superpower: '' };
  
  charactersMetadata[charType].forEach(metadatum => {
    switch(metadatum.type) {
      case 'text': // blank if input type text
        defaultModelValue[metadatum.key] = '';
      break;
      case 'dropdown': // let the default value is the first item of options
        defaultModelValue[metadatum.key] = metadatum.options[0].key
      break;
      default: // such is id
        defaultModelValue[metadatum.key] = null
      break;
    }
  });

  return defaultModelValue;
}
