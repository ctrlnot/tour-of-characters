export class Character {
  id: number;
  name: string;
  superpower: string;
  rating?: string;
}
