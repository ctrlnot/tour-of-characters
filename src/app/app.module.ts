import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppRoutingModule } from './routes/app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CharacterListComponent } from './components/dashboard/character-list/character-list/character-list.component';
import { CharacterSearchComponent } from './components/dashboard/search/character-search/character-search.component';

import { CharactersComponent } from './components/characters/characters.component';
import { CharactersTableComponent } from './components/characters/characters-table/characters-table.component';
import { CharacterDetailComponent } from './components/characters/character-detail/character-detail.component';
import { AddCharacterComponent } from './components/characters/forms/add-character/add-character.component';
import { CharacterDetailsFormComponent } from './components/characters/forms/character-details-form/character-details-form.component';

import { SpinnerComponent } from './components/spinner/spinner.component';
import { NotFoundComponent } from './components/errors/not-found/not-found.component';
import { ConfirmPopupComponent } from './components/popups/confirm-popup/confirm-popup.component';
import { SmallSpinnerComponent } from './components/spinner/small-spinner/small-spinner.component';

import { CharactersService } from './services/characters/characters.service';
import { CharacterFormsService } from './services/characters/character-forms.service';
import { PopUpService } from './services/popup.service';

@NgModule({
  declarations: [
    HeaderComponent,
    AppComponent,
    DashboardComponent,
    CharacterListComponent,
    CharacterSearchComponent,
    CharactersComponent,
    CharactersTableComponent,
    CharacterDetailComponent,
    AddCharacterComponent,
    CharacterDetailsFormComponent,
    NotFoundComponent,
    ConfirmPopupComponent,
    SpinnerComponent,
    SmallSpinnerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    CharactersService,
    CharacterFormsService,
    PopUpService,
    FormBuilder
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
