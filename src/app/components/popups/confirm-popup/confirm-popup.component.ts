import { Component, OnDestroy, HostBinding } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { PopUpService } from './../../../services/popup.service';

@Component({
  selector: 'app-confirm-popup',
  templateUrl: './confirm-popup.component.html',
  styleUrls: ['./confirm-popup.component.css']
})
export class ConfirmPopupComponent implements OnDestroy {
  @HostBinding('class.active') isActive: boolean = false;

  message: string;
  onOkay: Function;
  onCancel: Function;

  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private popUpService: PopUpService) {
    this.popUpService.confirmationModal
      .takeUntil(this.ngUnsubscribe)
      .subscribe(args => {
        this.toggleIsActiveMode();
        this.message = args.message;
        this.onOkay = args.onOkay;
        this.onCancel = args.onCancel;
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  // run the add character function from the binded this from <character-details-form> component
  // then close the modal
  defaultOnOkay(): void {
    this.onOkay();
    this.toggleIsActiveMode();
  }

  // close the modal then disable the forms
  defaultOnCancel() {
    this.onCancel();
    this.toggleIsActiveMode();
  }

  // toggles the modal to display or not to be displayed
  // initially false
  toggleIsActiveMode(): void {
    this.isActive = !this.isActive;
  }
}
