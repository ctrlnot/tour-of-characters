import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { Character } from './../../models/character';

import { CharactersService } from './../../services/characters/characters.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit, OnDestroy {
  charType: string;
  characters: Character[] = [];
  filterInput = new FormControl();
  addModalIsActive: boolean = false;
  retrievingCharacters: boolean = false;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private charactersService: CharactersService
  ) { }

  // get the url param of character type
  // if exists.. retrieve all characters and initialize filter input
  // else the api redirects it to 404
  ngOnInit() {
    this.route.params.subscribe(urlCharType => {
      this.charType = urlCharType.charType;
      this.characters = [];
      this.getCharacters();

      // initialize input value change event
      this.filterInput
        .valueChanges
        .debounceTime(400)
        .distinctUntilChanged()
        .takeUntil(this.ngUnsubscribe)
        .subscribe(term => this.getCharacters(term));
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  // retrieve all specific types characters from api
  // redirect to 404 if charType param is not found
  // when no sort params is passed.. default is:
  // sort by ID in ASCENDING order
  getCharacters(term?: string, sortBy?: string, order?: string): void {
    this.retrievingCharacters = true;

    // return empty array if the input is an invalid regex
    // this is to prevent json-server err in request url
    if(term && !this.testFilterInput(term)) {
      this.characters = [];
      this.retrievingCharacters = false;
      return;
    }

    this.charactersService
      .getCharacters(this.charType, term, sortBy, order)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        characters => {
          this.characters = characters;
          this.retrievingCharacters = false;
        },
        error => {
          this.handleUnknownCharacterError(error);
        }
      );
  }

  // accepts sort params from the characters table event emitter
  // to use as parameters when getting characters list
  sortCharacters(sortParams: any): void {
    const currentFilterInputValue = this.filterInput.value || '';

    this.getCharacters(currentFilterInputValue, sortParams.property, sortParams.by);
  }

  // duh...
  deleteCharacter(character: Character) {
    this.retrievingCharacters = true;

    this.charactersService
      .deleteCharacter(character, this.charType)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(() => this.getCharacters());
  }

  // show the add character modal
  toggleAddCharacterModal(): void {
    this.addModalIsActive = !this.addModalIsActive;
  }

  // handles error when unknown character
  handleUnknownCharacterError(err): void {
    if(err.status === 404) {
      this.router.navigate([`/404`]);
    } else {
      console.log(err);
    }
  }

  testFilterInput(input: string): boolean {
    let patternValid = true;

    try {
      new RegExp(input)
    } catch (err) {
      console.log(err);
      patternValid = false;
    }

    return patternValid;
  }
}
