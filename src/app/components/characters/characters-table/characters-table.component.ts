import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Character } from './../../../models/character';
import { getCharacterKeys } from './../../../models/characters-metadata';
import { getCharacterMetadata } from './../../../models/characters-metadata';

import { CharactersService } from './../../../services/characters/characters.service';
import { PopUpService } from './../../../services/popup.service';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';

@Component({
  selector: 'characters-table',
  templateUrl: './characters-table.component.html',
  styleUrls: ['./characters-table.component.css']
})
export class CharactersTableComponent implements OnChanges {
  @Input() charType: string;
  @Input() characters: Character[];
  @Output() emitSortCharacters = new EventEmitter<any>();
  @Output() emitDeleteCharacter = new EventEmitter<any>();

  tableHeaders: any[];
  currentSort: { property: string, by: string };
  defaultSort = { property: 'id', by: 'asc' };

  constructor(
    private route: ActivatedRoute,
    private popUpService: PopUpService,
    private charactersService: CharactersService
  ) { }

  ngOnChanges(simpleChanges: SimpleChanges) {
    // update the character properties when changing character type
    if(this.charType) {
      this.tableHeaders = getCharacterKeys(this.charType);
    }

    // set the sorted params to default if the characters length is 0
    // which happens when changing character type
    if(this.characters.length === 0) {
      this.currentSort = Object.assign({}, this.defaultSort);
    }
  }
  
  // show modal confirmation if the user wants to delet da character
  // set the 2nd param as inline function to emit the deletion of character
  confirmDelete(character: Character) {
    this.popUpService.showConfirmation(
      `Are you sure you want to erase ${character.name} from existence?`,
      () => this.emitDeleteCharacter.emit(character)
    )
  }

  // sort method that emits sort characters method to the parent
  // passes the current sorted config to insert to the params of sort characters to the parent
  sortBy(property: string) {
    this.currentSort.by = property !== this.currentSort.property ? 'asc' : 
                                       this.currentSort.by === 'asc' ? 'desc' : 'asc';
    this.currentSort.property = property;
    
    this.emitSortCharacters.emit(this.currentSort);
  }

  // displays the appropriate arrow symbol on header
  getSortSymbol(property: string) {
    return this.currentSort && this.currentSort.property !== property ? '🡙' : this.currentSort.by === 'asc' ? '🡑' : '🡓';
  }
}
