import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { Character } from './../../../../models/character';
import { CharacterBaseForm } from './../../../../models/forms/character-base-form';
import { getCharacterMetadata } from './../../../../models/characters-metadata';
import { getCharacterDefaultModelValue } from './../../../../models/characters-metadata';

import { CharactersService } from './../../../../services/characters/characters.service';
import { CharacterFormsService } from './../../../../services/characters/character-forms.service';
import { PopUpService } from './../../../../services/popup.service';

@Component({
  selector: 'character-details-form',
  templateUrl: './character-details-form.component.html',
  styleUrls: ['./character-details-form.component.css']
})
export class CharacterDetailsFormComponent implements OnInit, OnDestroy {
  @Input() char: Character;
  @Input() charType: string;
  @Input() characters: Character[];
  @Output() emitResetCharactersTable = new EventEmitter<any>();
  @Output() emitCloseAddCharacterModal = new EventEmitter<any>();

  form: FormGroup;
  originalCharacterValue: Character;
  characterForms: CharacterBaseForm<any>[] = [];

  editMode: boolean = false;
  retrievingForms: boolean = false;
  isFormSubmitted: boolean = false;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(
    private charactersService: CharactersService,
    private cfs: CharacterFormsService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private popUpService: PopUpService,
  ) { }

  ngOnInit() {
    // if the invert boolean of character property is true
    // the forms for adding character mode
    // else.. for edit mode which is by default should be readonly initially
    const isFormReadOnlyInitially = !this.char;
    this.generateForms(isFormReadOnlyInitially);

    // If there's no input character.. set the char property to default instead of undefined
    if(!this.char) {
      this.char = getCharacterDefaultModelValue(this.charType)
    }

    // set the original value of character to another property
    // for the purpose of when discard the changes made
    this.setOriginalCharacterValue();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  // says the function itself :v run after identifying what character type
  generateForms(isEditing: boolean) {
    let metadata = getCharacterMetadata(this.charType);
    this.characterForms = this.cfs.generateForms(metadata);
    this.form = this.cfs.toFormGroup(this.characterForms);

    if(!isEditing) {
      this.form.disable();
    }
  }

  saveCharacter(): void {
    this.retrievingForms = true;

    this.charactersService
      .saveCharacter(this.char, this.charType)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(() => {
        // if there's character id (editing character)
        // go back to readonly after success
        // else.. close modal after the adding character success
        if(this.char.id) {
          this.toggleEditMode();
          this.setOriginalCharacterValue();
        } else {
          this.resetForms();
          this.emitCloseAddCharacterModal.emit();
        }

        this.resetTable();
        this.isFormSubmitted = false;
        this.retrievingForms = false;
      });
  }

  // set the original values to a variable to have a data to insert
  // when discarding the changes to the form
  // new discovery to remove the binding to the duplicate property
  setOriginalCharacterValue(): void {
    this.originalCharacterValue = Object.assign({}, this.originalCharacterValue, this.char);
  }

  // reset forms to blank (for adding new character)
  // input to blank, dropdown to first value
  resetForms(): void {
    let dropdownDefaultValues = {};

    // get all the dropdown forms
    const dropdownForms = this.characterForms.filter(form => {
      return form.controlType === 'dropdown';
    });

    // iterate each dropdown then get their dropdown first values then store to default values object
    for(let dropdownForm of dropdownForms) {
      dropdownDefaultValues[dropdownForm.key] = dropdownForm['options'][0].key
    }

    // reset whole form then set back the default value of dropdown(s)
    // this is to prevent the dropdown default value going to blank
    this.form.reset( dropdownDefaultValues );
  }

  // revert back the form to original values (for editing a character)
  revertForm(): void {
    this.form.reset(this.originalCharacterValue);
    this.isFormSubmitted = false;
  }

  // emit a resetCharactersTable function to general characters component to refresh the list of characters
  resetTable(): void {
    this.emitResetCharactersTable.emit();
  }

  // go back to last character type
  goBack(): void {
    this.router.navigate([`/characters/${this.charType}`]);
  }

  // is a form field valid :v
  isValid(characterForm: CharacterBaseForm<any>) {
    return this.form.controls[characterForm.key].valid;
  }

  // get all the error messages if there's
  errorMessages(characterForm: CharacterBaseForm<any>) {
    let errorMessages = [];
    const label = characterForm.label;
    const form = this.form.controls[characterForm.key];

    if(form.errors) {
      if(form.errors.required) {
        errorMessages.push(`${label} is required!`);
      }

      if(form.errors.pattern) {
        errorMessages.push(`Invalid ${label}!`);
      }
    }

    return errorMessages;
  }

  // function that triggers the edit mode of form details
  // which is initially false (for viewing purposes only)
  toggleEditMode() {
    this.editMode = !this.editMode;
    this.editMode ? this.form.enable() : this.form.disable();
  }

  // check the whole form if valid
  // responsible for enabling/disabling of the submit button
  get isFormValid() {
    return this.form.valid && !this.form.pristine && this.form.dirty;
  }

  // summons the modal confirmation to be confirmed by the user
  // if there is a character property.. it will update that character
  // else it will be added to the collections
  confirm(): void {
    this.isFormSubmitted = true;
    
    if(this.form.valid) {
      if(this.form.pristine) {
        this.popUpService.showConfirmation(
          'Congratulations, you did nothing!',
          undefined,
          this.toggleEditMode.bind(this)
        );
      } else {
        this.popUpService.showConfirmation(
          'Would you like to save that information?',
          // this.char ? this.updateCharacter.bind(this) : this.addCharacter.bind(this)
          this.saveCharacter.bind(this)
        );
      }
    }
  }

  // summons a modal that asks the user really wants to discard the changes (if there's any) 
  // if yes - bring back the original info then readmode
  // else - just close the modal still on editmode
  discard() : void {
    if(this.form.dirty) {
      this.popUpService.showConfirmation(
        'Do you want to discard your changes?',
        this.revertForm.bind(this)
      );
    } else {
      this.toggleEditMode();
    }
  }

  // modal confirmation before discarding add character forms
  discardAddCharacterModal() {
    if(this.form.dirty) {
      this.popUpService.showConfirmation(
        'Do you want to discard these information?',
        this.emitDiscardAddCharacterModal.bind(this)
      );
    } else {
      this.emitCloseAddCharacterModal.emit();
    }
  }

  emitDiscardAddCharacterModal() {
    this.resetForms();
    this.emitCloseAddCharacterModal.emit();
  }
}
