import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterDetailsFormComponent } from './character-details-form.component';

describe('CharacterDetailsFormComponent', () => {
  let component: CharacterDetailsFormComponent;
  let fixture: ComponentFixture<CharacterDetailsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterDetailsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterDetailsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
