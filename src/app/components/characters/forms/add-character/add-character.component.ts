import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'add-character',
  templateUrl: './add-character.component.html',
  styleUrls: ['./add-character.component.css']
})
export class AddCharacterComponent {
  @Input() charType: string;
  @Output() emitRefreshCharactersTable = new EventEmitter<any>();
  @Output() emitCloseAddCharacterModal = new EventEmitter<any>();

  // reset the character table component child
  resetTable() {
    this.emitRefreshCharactersTable.emit();
  }

  // close modal to parent
  closeModal() {
    this.emitCloseAddCharacterModal.emit();
  }
}
