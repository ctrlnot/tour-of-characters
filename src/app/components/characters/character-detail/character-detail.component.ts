import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { Character } from './../../../models/character';

import { CharactersComponent } from './../characters.component';

import { CharactersService } from './../../../services/characters/characters.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css']
})
export class CharacterDetailComponent implements OnInit, OnDestroy {
  char: Character;
  charType: string;
  retrievingCharacter: boolean = false;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private charactersService: CharactersService,
    private charactersComponent: CharactersComponent
  ) { }

  ngOnInit(): void {
    // get charType from parent of id url
    this.route.parent.params
      .takeUntil(this.ngUnsubscribe)
      .subscribe(urlParentParams => {
        this.charType = urlParentParams.charType;
        
        // get the id param
        this.route.params
          .takeUntil(this.ngUnsubscribe)
          .subscribe(urlParam => {
          this.getCharacter(urlParam.id);
        });
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  // get specific character from the api via id
  getCharacter(id: number) {
    this.retrievingCharacter = true;

    this.charactersService
      .getCharacter(id, this.charType)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        char => {
          this.char = char;
          this.retrievingCharacter = false;
        },
        error => {
          this.handleUnknownCharacterError(error);
        }
      );
  }

  // reset the characters component list
  refreshCharactersTable() {
    this.charactersComponent.getCharacters();
  }

  // handles error when unknown character
  handleUnknownCharacterError(err): void {
    if(err.status === 404) {
      this.router.navigate([`/404`]);
    } else {
      console.log(err);
    }
  }
}
