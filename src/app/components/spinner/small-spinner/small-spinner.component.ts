import { Component } from '@angular/core';

@Component({
  selector: 'small-spinner',
  templateUrl: './small-spinner.component.html',
  styleUrls: ['./small-spinner.component.css']
})
export class SmallSpinnerComponent { }
