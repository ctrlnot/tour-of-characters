import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { getAllCharacterTypes } from './../../models/characters-metadata';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  title = 'Tour of Random Characters?';
  characterTypes: any[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.characterTypes = getAllCharacterTypes();
  }
}
