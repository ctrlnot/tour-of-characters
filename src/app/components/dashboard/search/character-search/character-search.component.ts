import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';

import {
  debounceTime,
  distinctUntilChanged,
  switchMap
} from 'rxjs/operators';

import { Character } from './../../../../models/character';
import { CharactersService } from './../../../../services/characters/characters.service';

@Component({
  selector: 'app-character-search',
  templateUrl: './character-search.component.html',
  styleUrls: ['./character-search.component.css']
})
export class CharacterSearchComponent implements OnInit {
  @Input() characterTypes: any[];

  searchInput: string = '';
  charType: string;
  characters$: Observable<Character[]>;
  private searchCharacterTerms = new Subject<string>();

  constructor(private charactersService: CharactersService) { }

  ngOnInit(): void {
    this.charType = this.characterTypes[0];
    this.setSearchCharacterTerms();
  }

  // search character via term 
  searchChar(term: string): void {
    this.searchCharacterTerms.next(term); 
  }

  // change character type to search when category dropdown changed
  changeCharType(type: string) {
    this.charType = type;
  }

  // set search terms subject
  setSearchCharacterTerms(): void {
    this.characters$ = this.searchCharacterTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new observable each time the term changes
      switchMap((term: string) => this.charactersService.searchCharacters(term, this.charType))
    );
  }
}
