import {
  Component,
  OnInit,
  OnDestroy,
  Input
} from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { Character } from './../../../../models/character';

import { CharactersService } from './../../../../services/characters/characters.service';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit, OnDestroy {
  @Input() charType: string;
  characters: Character[];

  retrievingCharacters: boolean = false;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private characterService: CharactersService) { }

  ngOnInit() {
    this.retrievingCharacters = true;

    // get first 4 characters from api
    this.characterService
      .getCharacters(this.charType)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(characters => {
        this.retrievingCharacters = false;
        this.characters = characters.slice(0, 4);
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
