import { Component, OnInit } from '@angular/core';

import { getAllCharacterTypes } from './../../models/characters-metadata';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  characterTypes: any[] = [];

  ngOnInit(): void {
    this.characterTypes = getAllCharacterTypes();
  }
}
