import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PopUpService {
  confirmationModal: Subject<any> = new Subject();

  // upon calling.. set the message, on okay function and on cancel function to the pop up component
  showConfirmation(
    message: string,
    onOkay?: Function,
    onCancel?: Function
  ) {
    this.confirmationModal.next({
      message: message,
      onOkay: onOkay,
      onCancel: onCancel
    });
  }
}
