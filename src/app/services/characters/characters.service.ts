import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { Character } from './../../models/character';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable()
export class CharactersService {
  constructor( private http: HttpClient ) { }

  private getCharacterApiUrl(charType: string) {
    return `http://localhost:3000/${charType}`;
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      console.log(error);

      return of(result as T);
    }
  }

  getCharacter(id: number, charType: string): Observable<Character> {
    const url = `${this.getCharacterApiUrl(charType)}/${id}`;
    return this.http
      .get<Character>(url)
      .pipe(
        catchError(this.handleError<any>('getCharacter'))
      );
  }

  getCharacters(
    charType: string,
    term: string = '',
    sortBy: string = 'id',
    order: string = 'asc'
  ): Observable<Character[]> {
    const apiUrl = `${this.getCharacterApiUrl(charType)}`;
    const params = `?name_like=${term}&_sort=${sortBy}&_order=${order}`;
    
    return this.http
      .get<Character[]>(`${apiUrl + params}`)
      .pipe(
        catchError(this.handleError<any>('getCharacters'))
      );
  }

  saveCharacter(char: Character, charType: string): Observable<any> {
    const characterApi = this.getCharacterApiUrl(charType);
    const appendedId = char.id ? `/${char.id}` : '';
    const url = characterApi + appendedId;
    
    return char.id ?
      this.http
        .put(url, char, httpOptions)
        .pipe(
          catchError(this.handleError<any>('saveCharacter'))
        )
      :
      this.http
        .post(url, char, httpOptions)
        .pipe(
          catchError(this.handleError<any>('saveCharacter'))
        )
      ;
  }

  deleteCharacter(char: Character | number, charType: string): Observable<Character> {
    const id = typeof char === 'number' ? char : char.id;
    const url = `${this.getCharacterApiUrl(charType)}/${id}`;

    return this.http
      .delete<Character>(url, httpOptions)
      .pipe(
        catchError(this.handleError<Character>('deleteCharacter'))
      );
  }

  // get characters by input terms for characters table
  getCharactersByFilter(term: string, charType: string): Observable<Character[]> {
    const url = `${this.getCharacterApiUrl(charType)}/?name_like=${term}`;

    return this.http
      .get<Character[]>(url)
      .pipe(
        catchError(this.handleError<Character[]>('getCharactersByFilter'))
      );
  }

  // search characters by term.. return empty if nothing found
  searchCharacters(term: string, charType: string): Observable<Character[]> {
    if(!term.trim()) return of([]);
    
    const url = `${this.getCharacterApiUrl(charType)}/?name_like=${term}`;

    return this.http
      .get<Character[]>(url)
      .pipe(
        catchError(this.handleError<Character[]>('searchCharacters'))
      );
  }

}
