import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { CharacterBaseForm } from '../../models/forms/character-base-form';
import { CharacterTextboxForm } from '../../models/forms/character-textbox-form';
import { CharacterDropdownForm } from '../../models/forms/character-dropdown-form';

@Injectable()
export class CharacterFormsService {
  toFormGroup(characterForms: CharacterBaseForm<any>[]) {
    let group: any = {};

    characterForms.forEach(characterForm => {
      let validators = [];

      if (characterForm.required) { validators.push(Validators.required) }
      
      // just assumed that the pattern validation is not blank
      if (characterForm.validationPattern) { validators.push(Validators.pattern(characterForm.validationPattern)) }

      // set the first dropdown as default value
      if (characterForm.controlType === 'dropdown') { characterForm.value = characterForm['options'][0].key }

      group[characterForm.key] = new FormControl(characterForm.value || '', validators);
    });

    return new FormGroup(group);
  }

  generateForms(forms: any[]) {
    let generatedForm: CharacterBaseForm<any>[] = [];

    for(let form in forms) {
      const f = forms[form];

      switch(f.type) {
        case 'text':
          generatedForm.push(new CharacterTextboxForm(f));
        break;

        case 'dropdown':
          generatedForm.push(new CharacterDropdownForm(f));
        break;

        default:
          // skip :v (e.g hidden)
        break;
      }
    }

    return generatedForm;
  }
}
